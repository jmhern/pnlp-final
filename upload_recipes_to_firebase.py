import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

import json
import tqdm

if __name__ == "__main__":
    cred = credentials.Certificate('./pnlp-chef-firebase-adminsdk-l5mxv-fa00474837.json')
    firebase_admin.initialize_app(cred)

    db = firestore.client()

    with open('recipes_db.json', 'r') as mf:
        recipes = json.load(mf)

        for recipe in tqdm.tqdm(recipes):
            doc_ref = db.collection('recipes').document(recipe['name'])
            doc_ref.set(recipe)
