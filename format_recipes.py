import json

def format_recipes(recipes):
    recipes = list(recipes.values())
    for recipe in recipes:
        recipe['steps'] = [s['text'] for s in recipe['steps']]
        recipe['ingredients'] = [i['name'] for i in recipe['ingredients']]
    
    with open('recipes_db.json', 'w+') as mf2:
        json.dump(recipes, mf2)

if __name__ == "__main__":

    with open('recipes.json', 'r') as mf:
        format_recipes(json.load(mf))
