from algoliasearch.search_client import SearchClient
from auth import ALGOLIA_API_KEY, ALGOLIA_APP_ID
import json

if __name__ == "__main__":
    client = SearchClient.create(ALGOLIA_APP_ID, ALGOLIA_API_KEY)
    index = client.init_index('recipes')

    with open('recipes_db.json') as mf:
        recipes = json.load(mf)

        index.save_objects(recipes, {
            'autoGenerateObjectIDIfNotExist': bool
            # any other requestOptions
        })


