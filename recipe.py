from typing import List

class Ingredient(object):
    def __init__(self, name: str, measurement: str=''):
        self.ingredient_name = name
        self.amount = measurement

    def __iter__(self):
        yield 'name', self.ingredient_name
        yield 'amount', self.amount

class Step(object):
    def __init__(self, text: str):
        self.text = text
    
    def __iter__(self): 
        yield 'text', self.text
class Recipe(object):
    def __init__(self, recipe_name: str, steps, equipment: List[str], ingredients: List[Ingredient]):
        self.name = recipe_name
        self.steps = steps
        self.equipment = equipment
        self.ingredients = ingredients

    def __iter__(self):
        yield 'name', self.name
        yield 'steps', [dict(step) for step in self.steps]
        yield 'equipment', self.equipment
        yield 'ingredients', [dict(ing) for ing in self.ingredients]