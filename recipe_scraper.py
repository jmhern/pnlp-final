import requests as req
from bs4 import BeautifulSoup, Tag
import json
from typing import List
from recipe import *


def get_recipe_links(soup: BeautifulSoup) -> List[str]:
    comp_rivers = soup.find_all('li', class_='component-river-item')
    
    for r in comp_rivers:
        recipe_header = r.find('h1', class_='feature-item-hed').find('a')
        yield recipe_header.contents[0], recipe_header.get('href')

def get_recipe(recipe_name, recipe_url):
    response = req.get(recipe_url)
    soup = BeautifulSoup(response.content, features='html5lib')

    ing, equip, steps = soup.find('div', class_='content-group').find_all('div', class_='row')[:3]

    # grabs the text form of each ingredient
    ing = [Ingredient(ingredient.contents[0]) for ingredient in ing.find_all('div', class_='ingredients__text')]
    # grabs the text form of each piece of equipment
    equip = [equipment.contents[1] for equipment in equip.find_all('div', class_='image-grid-item-text')]
    # grabs the text form of every step
    # have to filter out some pesky 'react-text' chunks
    steps = [[c for c in step.find('p').contents if 'react' not in c] 
                for step in steps.find_all('li', class_='step')]

    # some steps will contain Tags within their contents
    # these Tags are typically <strong> tags
    # we could possibly leverage this if we wanted to
    for i, step in enumerate(steps):
        for j, txt in enumerate(step):
            if isinstance(txt, Tag):
                # replace the Tag by its text content
                step[j] = txt.contents[0]

        steps[i] = Step(''.join(step))

    return Recipe(recipe_name, steps, equip, ing)

def recipe_gen(recipe_links):
    for recipe_name, recipe_endpoint in recipe_links:
        yield get_recipe(recipe_name, f'{BASE_URL}{recipe_endpoint}')

if __name__ == '__main__':
    BASE_URL = 'https://www.bonappetit.com'
    RECIPE_LIST_URL = f'{BASE_URL}/basically/recipes/page'
    PAGE_LIM = 8

    json_data = {}
    for page_num in range(1, PAGE_LIM + 1):
        recipes_url = f'{RECIPE_LIST_URL}/{page_num}' 
        response = req.get(recipes_url)

        if response.status_code == 200:
            soup = BeautifulSoup(response.content, features='html5lib')
            recipes = recipe_gen(get_recipe_links(soup))
            for recipe in recipes:
                json_data[recipe.name] = dict(recipe)

        print('Done with page {}'.format(page_num))
    with open('recipes.json', 'w') as out:
        json.dump(json_data, out)
            
            
